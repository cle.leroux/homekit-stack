from dsmr_parser import telegram_specifications
from dsmr_parser.clients import SerialReader, SERIAL_SETTINGS_V5
from prometheus_client import start_http_server, Gauge

# 1 read serial info
serial_reader = SerialReader(
    device='/dev/ttyUSB0',
    serial_settings=SERIAL_SETTINGS_V5,
    telegram_specification=telegram_specifications.V4
)

g = Gauge('current_electricity_usage', 'current_electricity_usage')
h = Gauge('electricity_used_tarif_1', 'electricity_used_tarif_1')
i = Gauge('electricity_used_tarif_2', 'electricity_used_tarif_2')

def send_data(telegram):
    # Current Electricity usage
    g.set(float(telegram.CURRENT_ELECTRICITY_USAGE.value))

    # On/Off hours consumption
    h.set(float(telegram.ELECTRICITY_USED_TARIFF_1.value))

    # On/Off hours consumption
    i.set(float(telegram.ELECTRICITY_USED_TARIFF_2.value))
    
    
def run(serial_reader):
    for telegram in serial_reader.read_as_object():
        send_data(telegram)
    
if __name__ == '__main__':
    # Start up the server to expose the metrics.
    start_http_server(9367)
    run(serial_reader)
        