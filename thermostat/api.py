from requests import post

url = "http://10.0.0.13:8123/api/services/"
headers = {"Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJjNDA0ZThlZWU1MTI0MWE5OTYzZjgwNDdkMTU1ODlhMSIsImlhdCI6MTcwNDU1NjA2MiwiZXhwIjoyMDE5OTE2MDYyfQ.lg1QgUbBzf96S226mB3dseEWuFqvx24Pc-NOSwshh-Y"}
trvzb = {"device_id": "23ebc18acdd3c84c35669f0e25168fd8", "temperature" : 10}
switch = {"device_id": "2b05254d6a1a323c36c200926e0a70eb"}


def set_hf(mode):
    device = "climate/set_temperature"
    if mode == 'on':
        trvzb["temperature"] = 35
    else:
        trvzb["temperature"] = 10
    response = post(url + device, headers=headers, json=trvzb)
    print(response.text)

def set_pump(mode):
    set_on = "switch/turn_on"
    set_off = "switch/turn_off"
    if mode == 'on':
        response = post(url + set_on, headers=headers, json=switch)
    else:
        response = post(url + set_off, headers=headers, json=switch)
    print(response.text)
